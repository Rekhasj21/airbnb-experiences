import React from 'react'
import Navbar from './assets/Navbar'
import PhotGrid from './assets/PhotGrid'
import Card from './assets/Card'
import data from './data'
import './App.css'

function App() {
  const cards = data.map(item => <Card key={item.id} {...item} />)
  return (
    <div className='app-container'>
      <Navbar />
      <PhotGrid />
      <div className='cards-list'>
        {cards}
      </div>
    </div>
  )
}

export default App
