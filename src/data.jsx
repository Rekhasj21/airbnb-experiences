export default [
    {
        id: 1,
        title: "Life Lessons with Katie Zaferes",
        price: 136,
        coverImg: "/images/katie-zaferes.png",
        rating: 5.0,
        review: 6,
        location: "USA",
        openSpots: 0,
    },
    {
        id: 2,
        title: "Learn Wedding Photography",
        price: 125,
        coverImg: "/images/wedding-photography.png",
        rating: 5.0,
        review: 30,
        location: "USA",
        openSpots: 27,
    },
    {
        id: 3,
        title: "Group Mountain Biking",
        price: 50,
        coverImg: "/images/mountain-bike.png",
        rating: 4.8,
        review: 2,
        location: "Norway",
        openSpots: 3,
    }
]