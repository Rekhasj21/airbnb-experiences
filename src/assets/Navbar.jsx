import React from 'react'

export default function Navbar(){
    return(
        <nav className="navbar-container">
            <img src="/images/airbnb-logo.png" className="airbnb-logo"/>
        </nav>
    )
}