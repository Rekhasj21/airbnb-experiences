import React from 'react'

export default function Card(props) {
    const { title, price, coverImg, rating, review, location, openSpots } = props

    let buttonText
    if (openSpots === 0) {
        buttonText = 'SOLD OUT'
    }
    else if (openSpots > 5) {
        buttonText = 'ONLINE'
    }

    return (
        <div className='card-container'>
            {buttonText && (<div className='card-button'>{buttonText}</div>)}
            <img src={coverImg} className='card-image' />
            <div className="details-card-container">
                <img src="/images/star.png" className='star-image' />
                <span >{rating}</span>
                <span className='grey-color'>({review}) • </span>
                <span className='grey-color'>{location}</span>
            </div>
            <p className='card-title'>{title}</p>
            <p className='card-price'><span className='bold'>From ${price}</span> / person</p>
        </div>
    )
}