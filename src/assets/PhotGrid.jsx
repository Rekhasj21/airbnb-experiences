import React from 'react'

export default function PhotGrid() {
    return (
        <div className='photogrid-container'>
            <img src='/images/photo-grid.png' className='image' />
            <h1 className='image-heading'>Online Experience</h1>
            <p className='image-details'>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home</p>
        </div>
    )
}